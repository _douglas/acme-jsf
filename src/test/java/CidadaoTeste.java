import javax.persistence.EntityManager;

import dao.JPAUtil;
import model.Cidadao;


public class CidadaoTeste {

	public static void main(String[] args) {
			
		Cidadao cidadao = new Cidadao();
		cidadao.setNome("Enzo Seraphim");
		cidadao.setCep("05677888");
		cidadao.setCpf("09888777890");
		cidadao.setEndereco("Avenida Faria Dutra");
		cidadao.setMunicipio("Sorocaba");
		
		EntityManager em = new JPAUtil().getEntityManager();
		
		em.getTransaction().begin();
		em.persist(cidadao);
		
		em.getTransaction().commit();
		em.close();

	}

}
