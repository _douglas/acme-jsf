package model;

import java.util.Iterator;

public class CadastroDePessoaNoTime {

	private Pessoa pessoa;
	private Time time;

	public void cadastraPessoaNoTime() {
		time.addPessoa(pessoa);
	}

	public Pessoa removePessoaDoTime() {
		Iterator<Pessoa> it = time.iteratorPessoa();
		while (it.hasNext()) {
			Pessoa p = it.next();
			if (p.getCpf().equalsIgnoreCase(pessoa.getCpf())) {
				it.remove();
				return p;
			}
		}
		return null;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}
	
	
}
