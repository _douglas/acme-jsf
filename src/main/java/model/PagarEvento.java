package model;

public class PagarEvento implements Comando {

	InscricaoPessoaEvento ipenv;

	public PagarEvento(InscricaoPessoaEvento ipenv) {
		this.ipenv = ipenv;
	}

	public void executa() throws Exception{
		ipenv.inscrever();
	}

}
