package model;

import java.util.ArrayList;
import java.util.List;

public class FilaDeInstrucoes {

	List<Comando> comandos;
	
	public FilaDeInstrucoes(){
		comandos = new ArrayList<Comando>();
	}
	
	public void addComando(Comando cmd){
		comandos.add(cmd);
	}
	
	public void processa() throws Exception{
		for(Comando comando : comandos){
			comando.executa();
		}
	}
}
