package model;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("evento")
public class Evento extends ProgramaPublico {

	private double taxaDeInscricao;
	
	@OneToMany
	private List<Local> locais = new ArrayList<Local>();
	private int vagasDisponiveis;
	
	public int getVagasDisponiveis() {
		return vagasDisponiveis;
	}

	public void setVagasDisponiveis(int vagasDisponiveis) {
		this.vagasDisponiveis = vagasDisponiveis;
	}

	public double getTaxaDeInscricao() {
		return taxaDeInscricao;
	}

	public void setTaxaDeInscricao(double taxaDeInscricao) {
		this.taxaDeInscricao = taxaDeInscricao;
	}

	public void addLocal(Local local) {
		locais.add(local);
	}

	public Local removeLocal(int index) {
		return locais.remove(index);
	}

	public Iterator<Local> iteratorLocal() {
		return locais.iterator();
	}
	
	public Local findLocal(String nomeDoLocal){
		for(Local l : locais){
			if(l.getNome().equalsIgnoreCase(nomeDoLocal)){
				return l;
			}
		}
		return null;
	}
}
