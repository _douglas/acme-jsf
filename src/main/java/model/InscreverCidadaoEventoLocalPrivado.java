package model;

import java.util.HashMap;

public class InscreverCidadaoEventoLocalPrivado {
	private Cidadao cidadao;
	private Evento evento;
	private LocalPrivado local;
	private HashMap<Evento, Cidadao> inscricao = new HashMap<Evento, Cidadao>();
	
	private boolean eventoOcorreNoLocal(){
		return (evento.findLocal(local.getNome()) != null) ? true : false; 
	}
	
	private boolean eventoTemVagasDisponiveis(){
		return (evento.getVagasDisponiveis() > 0) ? true : false;
	}

	public void inscrever() throws Exception {

		if(eventoOcorreNoLocal() && eventoTemVagasDisponiveis()){
			inscricao.put(evento, cidadao);
			cidadao.paga(local.getPreco());
		}
		else throw new Exception();		
	}
	
}
