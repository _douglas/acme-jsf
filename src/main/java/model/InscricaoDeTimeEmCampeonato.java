package model;

import java.util.Iterator;

public class InscricaoDeTimeEmCampeonato {
	
	private Time time;
	private Campeonato campeonato;
	
	public void inscreveTimeEmCampeonato(){
		campeonato.addTime(time);
	}
	
	public Time removeTimeDoCampeonato(){
		Iterator<Time> it = campeonato.iteratorTime();
		while(it.hasNext()){
			Time t = it.next();
			if(t.getNome().equalsIgnoreCase(time.getNome())){
				it.remove();
				return t;
			}
		}
		return null;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public Campeonato getCampeonato() {
		return campeonato;
	}

	public void setCampeonato(Campeonato campeonato) {
		this.campeonato = campeonato;
	}

	
}
