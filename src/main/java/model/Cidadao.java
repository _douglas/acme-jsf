package model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


import org.apache.log4j.Logger;

@Entity  
@DiscriminatorValue("cidadao")  
public class Cidadao extends Pessoa {
	
	private static Logger logger = Logger.getLogger(Cidadao.class);

	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		Cidadao.logger = logger;
	}

}
