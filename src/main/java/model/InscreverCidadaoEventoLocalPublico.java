package model;

import java.util.HashMap;

public class InscreverCidadaoEventoLocalPublico implements InscricaoPessoaEvento {

	private Cidadao cidadao;
	private Evento evento;
	private LocalPublico local;
	private HashMap<Evento, Cidadao> inscricao = new HashMap<Evento, Cidadao>();
	
	private boolean eventoOcorreNoLocal(){
		return (evento.findLocal(local.getNome()) != null) ? true : false; 
	}
	
	private boolean eventoTemVagasDisponiveis(){
		return (evento.getVagasDisponiveis() > 0) ? true : false;
	}

	public void inscrever() throws Exception {

		if(eventoOcorreNoLocal() && eventoTemVagasDisponiveis()){
			inscricao.put(evento, cidadao);
		}
		else throw new Exception();		
	}
	
	
}
