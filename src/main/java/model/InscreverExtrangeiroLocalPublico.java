package model;

import java.util.HashMap;

public class InscreverExtrangeiroLocalPublico implements InscricaoPessoaEvento {
	private Estrangeiro estrangeiro;
	private Evento evento;
	private LocalPublico local;
	private HashMap<Evento, Estrangeiro> inscricao = new HashMap<Evento, Estrangeiro>();

	private boolean eventoOcorreNoLocal() {
		return (evento.findLocal(local.getNome()) != null) ? true : false;
	}

	private boolean eventoTemVagasDisponiveis() {
		return (evento.getVagasDisponiveis() > 0) ? true : false;
	}

	public void inscrever() throws Exception {

		if (eventoOcorreNoLocal() && eventoTemVagasDisponiveis()) {
			estrangeiro.paga(evento.getTaxaDeInscricao());
			inscricao.put(evento, estrangeiro);
		} else
			throw new Exception();
	}
}
