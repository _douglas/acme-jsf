package model;

import java.util.Calendar;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Table;

@Entity
@DiscriminatorValue("agendamento_class")
public class Agendamento {
	
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	
	@OneToOne
	private Local local;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar data;
	
	private int horaEntrada;
	private int minEntrada;
	private int horaSaida;
	private int minSaida;
	
	public Local getLocal() {
		return local;
	}
	public void setLocal(Local local) {
		this.local = local;
	}
	public Calendar getData() {
		return data;
	}
	public void setData(Calendar data) {
		this.data = data;
	}
	public int getHoraEntrada() {
		return horaEntrada;
	}
	public void setHoraEntrada(int horaEntrada) {
		this.horaEntrada = horaEntrada;
	}
	public int getMinEntrada() {
		return minEntrada;
	}
	public void setMinEntrada(int minEntrada) {
		this.minEntrada = minEntrada;
	}
	public int getHoraSaida() {
		return horaSaida;
	}
	public void setHoraSaida(int horaSaida) {
		this.horaSaida = horaSaida;
	}
	public int getMinSaida() {
		return minSaida;
	}
	public void setMinSaida(int minSaida) {
		this.minSaida = minSaida;
	}

}
