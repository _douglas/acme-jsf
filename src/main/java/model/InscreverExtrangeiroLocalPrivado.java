package model;

import java.util.HashMap;

public class InscreverExtrangeiroLocalPrivado implements InscricaoPessoaEvento{
	private Estrangeiro estrangeiro;
	private Evento evento;
	private LocalPrivado local;
	private HashMap<Evento, Estrangeiro> inscricao = new HashMap<Evento, Estrangeiro>();

	private boolean eventoOcorreNoLocal() {
		return (evento.findLocal(local.getNome()) != null) ? true : false;
	}

	private boolean eventoTemVagasDisponiveis() {
		return (evento.getVagasDisponiveis() > 0) ? true : false;
	}

	public void inscrever() throws Exception {

		if (eventoOcorreNoLocal() && eventoTemVagasDisponiveis()) {
			estrangeiro.paga(evento.getTaxaDeInscricao());
			estrangeiro.paga(local.getPreco());
			inscricao.put(evento, estrangeiro);
		} else
			throw new Exception();
	}
}
