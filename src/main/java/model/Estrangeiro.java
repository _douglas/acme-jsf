package model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("estrangeiro")
public class Estrangeiro extends Pessoa {

	private String cidadeDeOrigem;
	private int tempoDePermanenciaEmDias;
	

	public int getTempoDePermanenciaEmDias() {
		return tempoDePermanenciaEmDias;
	}

	public void setTempoDePermanenciaEmDias(int tempoDePermanenciaEmDias) {
		this.tempoDePermanenciaEmDias = tempoDePermanenciaEmDias;
	}

	public String getCidadeDeOrigem() {
		return cidadeDeOrigem;
	}

	public void setCidadeDeOrigem(String cidadeDeOrigem) {
		this.cidadeDeOrigem = cidadeDeOrigem;
	}
	
}
