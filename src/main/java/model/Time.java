package model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Time {

	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	
	private String nome;
	
	@OneToOne
	private Esporte esporte;

	@OneToMany
	private List<Pessoa> membros = new ArrayList<Pessoa>();
	
	public Esporte getEsporte() {
		return esporte;
	}

	public void setEsporte(Esporte esporte) {
		this.esporte = esporte;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void addPessoa(Pessoa p) {
		membros.add(p);
	}

	public Pessoa removePessoa(int index) {
		return membros.remove(index);
	}

	public Iterator<Pessoa> iteratorPessoa() {
		return membros.iterator();
	}

	public Pessoa findPessoa(String nomeDaPessoa) {
		for (Pessoa p : membros) {
			if (p.getNome().equalsIgnoreCase(nomeDaPessoa))
				return p;
		}
		return null;
	}
}
