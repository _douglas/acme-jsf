package model;

import java.util.Iterator;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("esporte_class")
public class Esporte {

	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int id;
	
	private String modalidade;
	private String nome;
	private String numeroDeEsportistas;

	@OneToMany
	private List<Time> times;

	public String getModalidade() {
		return modalidade;
	}

	public void setModalidade(String modalidade) {
		this.modalidade = modalidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNumeroDeEsportistas() {
		return numeroDeEsportistas;
	}

	public void setNumeroDeEsportistas(String numeroDeEsportistas) {
		this.numeroDeEsportistas = numeroDeEsportistas;
	}

	private boolean isColetivo() {
		return times.isEmpty() ? true : false;
	}

	private void addTime(Time time) {
		try {
			times.add(time);
		} catch (Exception e) {
			System.out
					.println("Lista de times nula! � preciso instaciar a lista de times.");
		}
	}

	private Time removeTime(int index) {
		Time t = new Time();
		try {
			t = times.remove(index);
		} catch (Exception e) {
			System.out
					.println("Lista de times nula! � preciso instaciar a lista de times.");
		}
		return t;
	}

	private Iterator<Time> iteratorTime() {
		return times.iterator();
	}

	private Time findTime(String nomeDoTime) {
		for (Time t : times) {
			if (t.getNome().equalsIgnoreCase(nomeDoTime))
				return t;
		}
		return null;
	}
}
