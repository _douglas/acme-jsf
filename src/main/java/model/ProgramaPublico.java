package model;

import java.util.Calendar;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Programa_Public")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ProgramaPublico_Type", discriminatorType = DiscriminatorType.STRING)
public abstract class ProgramaPublico {
	
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar data;
	private String mandato;
	private double verbaGasta;
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public String getMandato() {
		return mandato;
	}

	public void setMandato(String mandato) {
		this.mandato = mandato;
	}

	public double getVerbaGasta() {
		return verbaGasta;
	}

	public void setVerbaGasta(double verbaGasta) {
		this.verbaGasta = verbaGasta;
	}

}
