package model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@DiscriminatorValue("campeonato") 
public class Campeonato extends Evento {

	@OneToOne
	private Esporte esporte;
	
	@OneToMany
	private List<Time> timesInscritos = new ArrayList<Time>();

	public Esporte getEsporte() {
		return esporte;
	}

	public void setEsporte(Esporte esporte) {
		this.esporte = esporte;
	}

	public void addTime(Time time) {
		timesInscritos.add(time);
	}

	public Time removeTime(int index) {
		return timesInscritos.remove(index);
	}

	public Iterator<Time> iteratorTime() {
		return timesInscritos.iterator();
	}

	public Time searchTime(String nomeDoTime) {
		for (Time t : timesInscritos) {
			if (t.getNome().equalsIgnoreCase(nomeDoTime))
				return t;
		}
		return null;
	}

}
