package controller;

import javax.faces.bean.ManagedBean;

import dao.DAO;
import model.Cidadao;

@ManagedBean
public class CidadaoBean {

	private Cidadao cidadao = new Cidadao();

	public Cidadao getCidadao() {
		return this.cidadao;
	}
	
	public void gravar(){
		System.out.println("Gravando cidad�o " + cidadao.getNome() + " no banco de dados.");
		new DAO<Cidadao>(Cidadao.class).adiciona(this.cidadao);
		System.out.println("Cidad�o " + cidadao.getNome() + " gravado no banco.");
		
		Cidadao cid = new Cidadao();
	}
}
