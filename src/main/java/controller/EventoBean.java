package controller;

import javax.faces.bean.ManagedBean;

import model.Evento;
import dao.DAO;

@ManagedBean
public class EventoBean {
	private Evento evento = new Evento();

	public Evento getEvento() {
		return this.evento;
	}

	public void gravar() {
		System.out.println("Gravando evento " + evento.getNome()
				+ " no banco de dados.");
		new DAO<Evento>(Evento.class).adiciona(this.evento);
		System.out.println("Evento " + evento.getNome() + " gravado no banco.");

		Evento cid = new Evento();
	}
}